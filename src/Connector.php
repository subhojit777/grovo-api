<?php

namespace acromedia\Grovo;

use acromedia\Grovo\OnboardingOption\OnboardingOptionInterface;
use acromedia\Grovo\Response\BadRequest;
use acromedia\Grovo\Response\Forbidden;
use acromedia\Grovo\Response\ResponseInterface;
use acromedia\Grovo\Response\Success;
use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\ClientException;

class Connector
{

    /**
     * HTTP client for sending requests to the API.
     *
     * @var \GuzzleHttp\ClientInterface
     */
    protected $client;

    /**
     * The type of onboarding process to use for these users.
     *
     * @var \acromedia\Grovo\OnboardingOption\OnboardingOptionInterface
     */
    protected $onboardingOption;

    /**
     * The URL for the Grovo API.
     *
     * @var string
     */
    protected $url;

    /**
     * A Grovo API key.
     *
     * @var string
     */
    protected $key;

    /**
     * Create a user connector.
     *
     * @param \GuzzleHttp\ClientInterface
     *   An HTTP client to send requests by.
     * @param \acromedia\Grovo\OnboardingOption\OnboardingOptionInterface $onboardingOption
     *   The type of onboarding to use for the user.
     * @param string $url
     *   The URL for the Grovo API endpoint.
     * @param string $key
     *   Your API key.
     */
    public function __construct(ClientInterface $client, OnboardingOptionInterface $onboardingOption, string $url, string $key)
    {
        $this->client = $client;
        $this->onboardingOption = $onboardingOption;
        $this->url = $url;
        $this->key = $key;
    }

    /**
     * Create a user connector with the default HTTP client.
     *
     * @param \acromedia\Grovo\OnboardingOption\OnboardingOptionInterface $onboardingOption
     *   The type of onboarding to use for the user.
     * @param string $url
     *   The URL for the Grovo API endpoint.
     * @param string $key
     *   Your API key.
     * @return \acromedia\Grovo\Connector
     *   A new connector to send users by.
     */
    public static function create(OnboardingOptionInterface $onboardingOption, string $url, string $key)
    {
        return new static(new Client(), $onboardingOption, $url, $key);
    }

    /**
     * Send users to the API.
     *
     * @param \acromedia\Grovo\User ...$users
     *   A list of users.
     *
     *   Call like `send($user1, $user2)` or `send(... $users_array)`.
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     *
     * @return \acromedia\Grovo\Response\ResponseInterface
     *   A response to the request to Grovo.
     */
    public function send(User ...$users): ResponseInterface
    {
        $encoded_users = implode("\n", array_map('json_encode', $users));
        try {
            $response = $this->client->request('POST', $this->url, [
              'headers' => [
                'x-api-key' => $this->key,
                'x-grovo-onboarding-option'=> $this->onboardingOption->getOption(),
                'Content-Type' => 'application/x-ndjson',
              ],
              'body' => $encoded_users,
            ]);
        } catch (ClientException $e) {
            if ($e->getCode() === 403) {
                return new Forbidden($e->getResponse());
            }
            return new BadRequest($e->getResponse());
        }
        return new Success($response);
    }
}
