<?php

namespace acromedia\Grovo\Response;

use Psr\Http\Message\ResponseInterface as PsrResponseInterface;

/**
 * A response from the Grovo API.
 */
class Response implements ResponseInterface
{

    /**
     * The HTTP response. Not available if a Guzzle exception was thrown.
     *
     * @var \Psr\Http\Message\ResponseInterface|null
     */
    protected $response;

    /**
     * @var
     */
    protected $errors = [];

    /**
     * Response constructor.
     *
     * @param \Psr\Http\Message\ResponseInterface $response
     *   The HTTP response from Grovo.
     */
    public function __construct(PsrResponseInterface $response)
    {
        $this->response = $response;
        $body = json_decode($this->response->getBody(), true);
        if (isset($body['errors'])) {
            $this->errors = array_map(function ($error) {
                return new Error($error);
            }, $body['errors']);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function successful(): bool
    {
        return $this->response->getStatusCode() >= 200 && $this->response->getStatusCode() <= 299;
    }

    /**
     * {@inheritdoc}
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

    /**
     * {@inheritdoc}
     */
    public function getResponse(): PsrResponseInterface
    {
        return $this->response;
    }
}
