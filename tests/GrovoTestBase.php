<?php

namespace acromedia\Grovo\tests;

use PHPUnit\Framework\TestCase;
use acromedia\Grovo\User;
use acromedia\Grovo\ReportsTo;
use acromedia\Grovo\DirectRole;
use acromedia\Grovo\HireDate;

/**
 * Common functions for a Grovo api test.
 */
abstract class GrovoTestBase extends TestCase
{
    /**
     * Get test users.
     *
     * @return \acromedia\Grovo\User[]
     *   A set of test users.
     */
    protected function getUsers()
    {
        $users[] = User::create('caroldanvers', 'caroldanvers@grovo.com', 'Carol', 'Danvers')
          ->active()
          ->setReportsTo(new ReportsTo(User::createPartial('123456')))
          ->addDirectRole(new DirectRole('Creator'))
          ->addDirectRole(new DirectRole('Learning Administrator'))
          ->addDirectRole(new DirectRole('Manager'))
          ->setHireDate(new HireDate(\DateTime::createFromFormat(\Datetime::ATOM, '1968-03-01T00:00:00+00:00')))
          ->set('city', 'Boston')
          ->set('country', 'USA')
          ->set('department', 'Operations')
          ->set('jobTitle', 'Super Hero')
          ->set('location', 'Headquarters')
          ->set('region', 'North America')
          ->set('state', 'Massachusetts');
        $users[] = User::create('joesmith', 'jsmith@example.com', 'Joe', 'Smith')
          ->active()
          ->setReportsTo(new ReportsTo($users[0]))
          ->addDirectRole(new DirectRole('Creator'))
          ->addDirectRole(new DirectRole('Learning Administrator'))
          ->setHireDate(new HireDate(\DateTime::createFromFormat(\DateTime::ATOM, '1968-03-01T00:00:00+00:00')))
          ->set('city', 'Boston')
          ->set('country', 'USA')
          ->set('department', 'Operations')
          ->set('jobTitle', 'Side Kick')
          ->set('location', 'Headquarters')
          ->set('region', 'North America')
          ->set('state', 'Massachusetts');
        return $users;
    }
}
